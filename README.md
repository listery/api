# listery/api

This API package is designed to integrate league/fractal tightly with the Laravel framework,
so that with minimal setup you can be up and running with an api.
It is designed to make it as easy to use as possible.

Author/Main Contact: craig@listery.io

## Setup

Go to `config/app.php` in your Laravel installation and add this to your `'providers'` section:
```
Listery\Api\ApiServiceProvider::class,
```

Next, go to `app/Http/Kernel.php` and change the class to extend from the Api package Kernel.
It should then look like this:
```php
<?php

namespace App\Http;
use Listery\Api\Http\Kernel as ApiHttpKernel;

class Kernel extends ApiHttpKernel
{
    //...
}
```

While in the Kernel class, be sure to add a middleware into the `$middleware` property to point to the
class `\Listery\Api\Http\Middleware\ReturnsJson::class`. The suggested middleware name is `'json'`. E.g:
```php
$middleware = [
    //...
    'json' => \Listery\Api\Http\Middleware\ReturnsJson::class,
    //...
];
```

Then add the middleware to any routes that you want transformed to the JSON api output. Remember, you can add middleware to a
middleware group so that it's activated on any 'api' route.

## Configuration

To modify the default configuration _(and you will definitely want to!)_, publish the config as so:

`php artisan vendor:publish --tag=listery --force`

Now, in the `config/api.php` file, you can add 'Transformers' to the transformers array.
They should map as `Transformer::class => TransformableEntity::class`. Alternatively, if the transformer
type-hints the `TransformableEntity::class` then it will auto-resolve and you do not need to specify it
beforehand.

Example:

```php
<?php 

return [
    'transformers' => [
        Transformer::class,
        AnotherTransformer::class => Entity::class,
        YetAnotherTransformer::class => [AnotherEntity::class, YetAnotherEntity::class],
    ]  
];
```

## Usage Instructions

Once you have specified your Transformers, you can return any type of response from your controller
actions.
For example, a single class, collection, paginator, string or null are possible values to return.

If you return any of these then it will be intercepted by this package and transformed appropriately if a
transformer exists for the entity/class.

For example:

```php
<?php

use App\Http\Controllers\Controller;

class IndexController extends Controller {

    public function index() {
        return collect(
            [
                new Foo(),
                new Foo(),
                new Foo()
            ]
        );
    }
}
```

would potentially output
```json
{
  "data": [
    {"name": "foo"},
    {"name": "foo"},
    {"name": "foo"}
  ]
}
```