<?php

use Listery\Api\Exceptions\Custom\ValidationException;
use Listery\Api\Exceptions\Handler;
use Listery\Api\Exceptions\Interceptors\ValidationExceptionInterceptor;
use Listery\Api\Http\Response\Adapters\CollectionAdapter;
use Listery\Api\Http\Response\Adapters\ItemAdapter;
use Listery\Api\Http\Response\Adapters\NullAdapter;
use Listery\Api\Http\Response\Adapters\PaginatorAdapter;
use Listery\Api\Http\Response\Adapters\StringAdapter;
use Listery\Api\Http\Response\Interceptors\Metadata\ExecutionTimeInterceptor;
use Listery\Api\Http\Response\Serializers\DefaultSerializer;

return [
    /*
     |------------------------------------------------------------
     | Transformers
     |------------------------------------------------------------
     |
     | Transformers are used to convert the response to the
     | appropriate format. They should return an array of data.
     | See http://fractal.thephpleague.com/transformers/ for more
     | details.
     |
     */
    'transformers' => [
    ],
    /*
     |------------------------------------------------------------
     | Custom Errors
     |------------------------------------------------------------
     |
     | Define custom errors here. They will be intercepted as
     | 'soft' errors and will be nested in the 'meta' tag
     | of the response with the appropriate code and message
     | array.
     |
     */
    'errors' => [
        // CustomException => CustomErrorCode
        ValidationException::class => 422,
    ],
    /*
     |------------------------------------------------------------
     | Metadata Interceptors
     |------------------------------------------------------------
     |
     | Each interceptor is run one after the other to add
     | metadata to the 'meta' key in the HTTP response.
     | The interceptors will be injected by the ApiServiceProvider
     | so feel free to inject any dependencies you need.
     |
     */
    'metadata_interceptors' => [
        ExecutionTimeInterceptor::class
    ],
    /*
     |------------------------------------------------------------
     | Exception Interceptors
     |------------------------------------------------------------
     |
     | When an exception is thrown in the application
     | it will be proxied through the below interceptors. If
     | they are compatible, they should morph the exception into
     | the appropriate CustomException.
     |
     | This allows you to catch exceptions and return custom errors
     | without overriding the default behaviour of the framework.
     |
     */
    'exception_interceptors' => [
        ValidationExceptionInterceptor::class,
    ],
    /*
     |------------------------------------------------------------
     | Adapters
     |------------------------------------------------------------
     |
     | Each adapter is used to match against a response type
     | (e.g. array, item, entity, string etc).
     |
     | The first compatible adapter for the response is used, so
     | be wary of the order of adapters.
     | Generally you should match specific cases first
     | (e.g. Illuminate\Support\Collection first, and 'array' last).
     |
     */
    'adapters' => [
        PaginatorAdapter::class,
        CollectionAdapter::class,
        StringAdapter::class,
        NullAdapter::class,
        ItemAdapter::class,
    ],
    /*
     |------------------------------------------------------------
     | Configuration
     |------------------------------------------------------------
     |
     | Change configuration values here to affect the way the
     | package builds up and responds to the outside world.
     |
     | 'default_serializer' => The default Fractal serializer to
     | use for outputting API responses.
     |
     | 'handle_exceptions' => Whether to parse custom exceptions
     | or to just throw them globally.
     |
     | 'exception_handler' => The default exception handler to use
     | in the framework. You can leave this as default and modify
     | App\Exceptions\Handler should you wish.
     |
     */
    'default_serializer' => DefaultSerializer::class,
    'handle_exceptions' => true,
    'exception_handler' => Handler::class,
];