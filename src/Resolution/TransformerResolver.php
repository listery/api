<?php

namespace Listery\Api\Resolution;

use League\Fractal\TransformerAbstract;

interface TransformerResolver
{
    /**
     |------------------------------------------------------------
     | Binding
     |------------------------------------------------------------
     |
     | An object-type should be bound to the given transformer.
     | When the object-type is resolved, it should return the
     | supplied transformer.
     |
     | Note that it should accept either an instance or a class
     | reference as $object parameter.
     * @param string | Object $object
     * @param string
     */
    public function bind($object, $objectTransformer);

    /**
     |------------------------------------------------------------
     | Resolution
     |------------------------------------------------------------
     |
     | Get the transformer for a given object.
     | The object may be an instance or just a class-reference.
     |
     * @param string | Object $object
     * @return TransformerAbstract | null
     */
    public function get($object);
}