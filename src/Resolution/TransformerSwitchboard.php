<?php


namespace Listery\Api\Resolution;


use League\Fractal\TransformerAbstract;

/**
 * Class TransformerSwitchboard
 * @package Listery\Api\Resolution
 *
 * Acts as a transformer decorator to transform individual objects into their
 * response format using the appropriate 'real' transformer for each object.
 */
class TransformerSwitchboard extends TransformerAbstract implements Switchboard
{
    /**
     * @var TransformerResolver
     */
    protected $resolver;

    /**
     * @var TransformerAbstract
     */
    protected $currentTransformer;

    public function __construct(TransformerResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function transform($response)
    {
        $transformer = $this->resolver->get($response);
        $this->currentTransformer = $transformer;

        if(!$transformer) {
            return $response;
        }
        
        $this->currentTransformer = $transformer;
        return $transformer->transform($response);
    }

    public function getAvailableIncludes()
    {
        if(! $this->currentTransformer) {
            return [];
        }

        return $this->currentTransformer->getAvailableIncludes();
    }

    public function getDefaultIncludes()
    {
        if(! $this->currentTransformer) {
            return [];
        }

        return $this->currentTransformer->getDefaultIncludes();
    }

    public function __call($name, $arguments)
    {
        return call_user_func([$this->currentTransformer, $name], ...$arguments);
    }
}