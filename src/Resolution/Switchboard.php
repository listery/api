<?php

namespace Listery\Api\Resolution;


/**
 * Class TransformerSwitchboard
 * @package Listery\Api\Resolution
 *
 * Acts as a transformer decorator to transform individual objects into their
 * response format using the appropriate 'real' transformer for each object.
 */
interface Switchboard
{
    public function transform($response);
}