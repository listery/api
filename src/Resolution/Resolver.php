<?php


namespace Listery\Api\Resolution;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class Resolver implements TransformerResolver
{
    protected $bindings = [];
    protected $autoResolvedBindings = [];
    protected $transformers = [];

    public function __construct(array $resolvableTransformers)
    {
        $this->transformers = $resolvableTransformers;
    }

    public function bind($object, $objectTransformer)
    {
        $this->bindings[$this->getKey($object)] = $objectTransformer;

        return $this;
    }

    public function get($object)
    {
        $transformerClass = $this->getTransformer($object);

        if(!$transformerClass)
        {
            return $this->autoResolve($object);
        }

        $resolver = $this->transformers[$transformerClass] ?? null;
        if(!$resolver)
        {
            return null;
        }

        return $resolver();
    }

    private function getKey($object)
    {
        $key = $object;
        if(is_object($object))
        {
            $key = get_class($object);
        }

        if($object instanceof Paginator)
        {
            $object = $object->items();
        }

        if(is_array($object) || $object instanceof Collection)
        {
            $item = array_first($object);
            if(is_object($item))
            {
                $key = get_class($item);
            } else {
                $key = $item;
            }
        }

        return $key;
    }

    private function getTransformer($object)
    {
        return $this->bindings[$this->getKey($object)] ?? null;
    }

    public function addAutoResolvers(array $resolvers)
    {
        $this->transformers = $this->transformers + $resolvers;

        foreach($resolvers as $transformer => $resolver)
        {
            $transformer = new \ReflectionClass($transformer);

            if(!$transformer->isSubclassOf(TransformerAbstract::class))
            {
                throw new \Exception("API Transformers must be of type " . TransformerAbstract::class . ", " . $transformer->getName() . " was given.");
            }

            $transformMethod = $transformer->getMethod('transform');
            $transformMethodParameters = $transformMethod->getParameters();

            if(count($transformMethodParameters) > 0)
            {
                $guess = $transformMethodParameters[0];
                $entityClass = $guess->getClass();

                if(!$entityClass) {
                    continue;
                }

                $entity = $entityClass->getName();

                $this->autoResolvedBindings[$entity] = $this->transformers[$transformer->getName()]();
            }
        }
    }

    private function autoResolve($class)
    {
        return $this->autoResolvedBindings[$this->getKey($class)] ?? null;
    }
}