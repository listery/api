<?php


namespace Listery\Api;


use Listery\Api\Http\Contracts\ResponseFactory;
use Listery\Api\Http\Response\Adapters\FractalAdapter;
use Listery\Api\Http\Response\Adapters\ResponseAdapter;
use Listery\Api\Http\Response\Interceptors\Metadata\MetadataInterceptor;
use Listery\Api\Http\Routing\Router as ApiTransformationRouter;
use Listery\Api\Providers\ErrorServiceProvider;
use Listery\Api\Resolution\Resolver;
use Listery\Api\Resolution\Switchboard;
use Listery\Api\Resolution\TransformerContainer;
use Listery\Api\Resolution\TransformerResolver;
use Listery\Api\Resolution\TransformerSwitchboard;
use Listery\Api\Traits\ApiConfig;
use Illuminate\Container\Container;
use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Serializer\SerializerAbstract;
use League\Fractal\TransformerAbstract;

class ApiServiceProvider extends ServiceProvider
{
    use ApiConfig;

    public function boot()
    {
        $this->publishes([
            self::getConfigPath() => config_path(self::getConfigName() . ".php")
        ], 'listery');
    }

    public function register()
    {
        $this->mergeConfigFrom(self::getConfigPath(), self::getConfigName());

        $this->bindTransformationManager();
        $this->bindTransformerResolver();
        $this->bindResponseInstance();
        $this->bindRouter();
        $this->bindAdapters();
        $this->bindMetadataInterceptors();

        if ($this->getConfig('handle_exceptions')) {
            $this->app->register(ErrorServiceProvider::class);
        }
    }

    public function bindTransformationManager()
    {
        $this->app->bind(SerializerAbstract::class, $this->getConfig('default_serializer'));
        $this->app->singleton(Manager::class, function (Container $app) {
            $manager = new Manager();
            $manager->setSerializer($app->make(SerializerAbstract::class));

            return $manager;
        });
    }

    public function bindResponseInstance()
    {
        $this->app->bind(Http\Response\ResponseFactory::class, function (Container $app) {
            return new Http\Response\ResponseFactory(
                $app->tagged(ResponseAdapter::class),
                $app->tagged(MetadataInterceptor::class),
                $this->getConfig('errors')
            );
        });
        $this->app->bind(ResponseFactory::class, Http\Response\ResponseFactory::class);
    }

    public function bindRouter()
    {
        /** @var Router $defaultRouter */
        $this->app->singleton(ApiTransformationRouter::class);
        $this->app->alias(ApiTransformationRouter::class, 'router');
    }

    public function bindTransformerResolver()
    {
        $this->app->singleton(Resolver::class, function (Container $app){
            $resolver = new Resolver([]);

            $this->getForcedTransformers()->each(function ($entities, $transformer) use ($resolver) {
                foreach ($entities as $entity) {
                    $resolver->bind($entity, $transformer);
                }
            });

            return $resolver;
        });

        // Only initialise after Resolver is created so that it can be created independent of any Transformer, whom may need it.
        $this->app->afterResolving(Resolver::class, function(Resolver $resolver) {
            $resolver->addAutoResolvers(
                $this->getToBeAutoResolved()->merge($this->getForcedTransformers()->mapWithKeys(function ($entities, $transformer) {
                    return [$transformer => $this->app->getBindings()[$transformer] ?? $this->defaultTransformerResolveClosure($transformer)];
                }))->toArray()
            );
        });

        $this->app->alias(Resolver::class, TransformerResolver::class);
        $this->app->bind(Switchboard::class, TransformerSwitchboard::class);
    }

    private function getToBeAutoResolved()
    {
        $transformers = $this->getConfig('transformers');

        $toBeAutoResolved = collect($transformers)->filter(function ($item, $key) {
            return is_int($key);
        })->mapWithKeys(function ($transformer) {
            return [$transformer => $this->app->getBindings()[$transformer] ?? $this->defaultTransformerResolveClosure($transformer)];
        });

        return $toBeAutoResolved;
    }

    private function getForcedTransformers()
    {
        $transformers = $this->getConfig('transformers');

        $forced = collect($transformers)->filter(function ($item, $key) {
            return !is_int($key) && class_exists($key);
        })->mapWithKeys(function ($entities, $transformer) {
            if (!is_array($entities)) {
                $entities = [$entities];
            }

            return [$transformer => $entities];
        });

        return $forced;
    }

    public function bindAdapters()
    {
        $adapter = $this->getConfig('adapters');
        foreach ($adapter as $a) {
            $this->app->tag($a, ResponseAdapter::class);
        }
    }

    public function bindMetadataInterceptors()
    {
        $interceptors = $this->getConfig('metadata_interceptors');
        foreach ($interceptors as $interceptor) {
            $this->app->tag($interceptor, MetadataInterceptor::class);
        }
    }

    private function defaultTransformerResolveClosure($transformer)
    {
        return function () use ($transformer) {
            return $this->app->make($transformer);
        };
    }


}