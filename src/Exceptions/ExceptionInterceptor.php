<?php


namespace Listery\Api\Exceptions;


use Listery\Api\Exceptions\Interceptors\Interceptor;
use Illuminate\Support\Collection;

class ExceptionInterceptor
{
    /**
     * @var Interceptor[] | Collection
     */
    protected $interceptors;

    public function __construct(array $interceptors)
    {
        $this->interceptors = new Collection($interceptors);
    }

    public function intercept(\Exception $e)
    {
        if($e instanceof CustomException)
        {
            return $e;
        }

        /** @var Interceptor $interceptor */
        $interceptor = $this->interceptors->first(function(Interceptor $interceptor) use($e) {
            return $interceptor->morphs($e);
        });

        if(! $interceptor)
        {
            return $e;
        }

        return $interceptor->intercept($e);
    }
}