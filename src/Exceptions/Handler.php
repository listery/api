<?php


namespace Listery\Api\Exceptions;


use Listery\Api\Exceptions\Traits\ErrorCodes;
use Listery\Api\Http\Contracts\ResponseFactory;
use Listery\Api\Http\Routing\Router;
use Illuminate\Contracts\Container\Container;
use \Exception as BaseException;

class Handler extends \App\Exceptions\Handler
{
    use ErrorCodes;

    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var ExceptionInterceptor
     */
    protected $exceptionInterceptor;

    /**
     * @var Router
     */
    protected $router;

    public function __construct(Container $container, array $errorMap, ResponseFactory $responseFactory, ExceptionInterceptor $exceptionInterceptor, Router $router)
    {
        parent::__construct($container);
        $this->errorMap = $errorMap;
        $this->responseFactory = $responseFactory;
        $this->exceptionInterceptor = $exceptionInterceptor;
        $this->router = $router;
    }

    public function render($request, BaseException $exception)
    {
        //morph to custom exception if needed
        $interceptedException = $this->exceptionInterceptor->intercept($exception);

        if($this->getErrorCode($interceptedException) && $this->router->shouldReturnJson($request))
        {
            if(! $interceptedException instanceof CustomException)
            {
                throw new BaseException("Custom API errors must implement ".CustomException::class. ", " . ($interceptedException ? get_class($interceptedException) : json_encode($interceptedException)) . " was given.");
            }

            return $this->responseFactory->error($interceptedException);
        }

        return parent::render($request, $exception);
    }
}