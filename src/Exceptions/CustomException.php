<?php


namespace Listery\Api\Exceptions;


interface CustomException
{
    public function getMessages();
    public function getContent();
    public function getHttpStatusCode();
}