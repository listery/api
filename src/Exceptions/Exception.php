<?php


namespace Listery\Api\Exceptions;


use Illuminate\Support\Collection;

abstract class Exception extends \Exception implements CustomException
{
    /**
     * @var array
     */
    protected $messages;

    /**
     * @var object | array | Collection
     */
    protected $content;

    public function __construct($messages = [], $content = null)
    {
        if(!is_array($messages))
        {
            $messages = [$messages];
        }

        $this->messages = $messages;
        $this->content = $content;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getContent()
    {
        return $this->content;
    }
}