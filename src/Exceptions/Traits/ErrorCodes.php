<?php


namespace Listery\Api\Exceptions\Traits;


trait ErrorCodes
{
    protected $errorMap = [];

    private function getErrorCode($exception)
    {
        $class = (new \ReflectionClass($exception))->getName();

        return array_get($this->errorMap, $class, false);
    }
}