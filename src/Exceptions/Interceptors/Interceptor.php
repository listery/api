<?php


namespace Listery\Api\Exceptions\Interceptors;


use Listery\Api\Exceptions\CustomException;

interface Interceptor
{
    /**
     * @param $exception
     * @return boolean
     */
    public function morphs($exception);

    /**
     * @param $exception
     * @return CustomException
     */
    public function intercept($exception);
}