<?php


namespace Listery\Api\Exceptions\Interceptors;


use Illuminate\Validation\ValidationException;

class ValidationExceptionInterceptor implements Interceptor
{
    public function morphs($exception)
    {
        return $exception instanceof ValidationException;
    }

    public function intercept($exception)
    {
        /** @var ValidationException $exception */
        return new \Listery\Api\Exceptions\Custom\ValidationException($exception);
    }

}