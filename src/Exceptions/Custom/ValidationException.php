<?php


namespace Listery\Api\Exceptions\Custom;



use Listery\Api\Exceptions\Exception;

class ValidationException extends Exception
{
    /**
     * @var \Illuminate\Validation\ValidationException
     */
    private $exception;

    public function __construct(\Illuminate\Validation\ValidationException $exception)
    {
        $this->exception = $exception;
        $this->messages = [];

        foreach($this->exception->validator->errors()->getMessages() as $field => $messages){
            foreach($messages as $message)
            {
                $this->messages[$field][] = $message;
            }
        }
    }

    public function getMessages()
    {
        $errors = collect($this->exception->validator->errors()->getMessages());
        return $errors->toArray();
    }

    public function getContent()
    {
        return null;
    }

    public function getHttpStatusCode()
    {
        return 422;
    }
}