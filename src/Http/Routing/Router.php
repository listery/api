<?php


namespace Listery\Api\Http\Routing;


use Listery\Api\Http\Contracts\ResponseFactory;
use Listery\Api\Http\Middleware\ReturnsJson;
use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;

class Router extends \Illuminate\Routing\Router implements Registrar
{
    /**
     * @var \Illuminate\Routing\Router
     */
    protected $router;

    public function prepareResponse($request, $response)
    {
        /** @var Request $request */

        if ($response instanceof PsrResponseInterface) {
            $response = (new HttpFoundationFactory)->createResponse($response);
        } elseif (!$response instanceof Response && $this->shouldReturnJson($request)) {
            $response = $this->container->make(ResponseFactory::class)->build($response); // if response not built and has appropriate middleware, build api response
        } elseif (!$response instanceof Response) {
            $response = new \Illuminate\Http\Response($response);
        }

        return $response->prepare($request);
    }

    public function shouldReturnJson(Request $request)
    {
        $keys = new Collection();

        foreach ($this->middleware as $key => $middleware) {
            if ($middleware === ReturnsJson::class) {
                $keys->push($key);
            }
        }

        if($request->route()) {
            $middlewareInUseByRequest = $keys->intersect($request->route()->gatherMiddleware());

            return $middlewareInUseByRequest->count() > 0;
        }

        if($request->expectsJson()) {
            return true;
        }

        return false;
    }
}