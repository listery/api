<?php


namespace Listery\Api\Http\Middleware;


use Closure;

class ReturnsJson
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}