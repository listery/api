<?php


namespace Listery\Api\Http\Contracts;


interface ResponseFactory
{
    public function build($response, $meta = null);
}