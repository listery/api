<?php


namespace Listery\Api\Http\Response\Interceptors\Metadata;


class ExecutionTimeInterceptor implements MetadataInterceptor
{
    public function run($response, $meta = [])
    {
        $timeNow = microtime(true);
        $executionTime = $timeNow - $_SERVER["REQUEST_TIME_FLOAT"];

        if(!$meta)
        {
            $meta = [];
        }

        return array_merge($meta, [
            'execution_time' => $executionTime
        ]);
    }
}