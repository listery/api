<?php


namespace Listery\Api\Http\Response\Interceptors\Metadata;


interface MetadataInterceptor
{
    /**
     * @param mixed $response
     * @param array | null $meta
     * @return array | null $metaModified
     */
    public function run($response, $meta = []);
}