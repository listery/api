<?php


namespace Listery\Api\Http\Response;

use Listery\Api\Exceptions\CustomException;
use Listery\Api\Exceptions\Traits\ErrorCodes;
use Listery\Api\Http\Response\Adapters\ResponseAdapter;
use Listery\Api\Http\Response\Interceptors\Metadata\MetadataInterceptor;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class ResponseFactory implements \Listery\Api\Http\Contracts\ResponseFactory
{
    use ErrorCodes;

    /**
     * @var array | ResponseAdapter[]
     */
    protected $adapters;

    /**
     * @var array
     */
    protected $metaInterceptors;

    public function __construct(array $adapters, array $metaInterceptors, array $errorMap)
    {
        $this->adapters = new Collection($adapters);
        $this->metaInterceptors = collect($metaInterceptors);
        $this->errorMap = $errorMap;
    }

    public function build($response, $meta = null)
    {
        $response = $this->runAdapters($response, $meta);

        $finalResp = new Response($response);

        return $finalResp;
    }

    public function error(CustomException $exception)
    {
        $meta = ['errors' => [ $this->getErrorCode($exception) => $exception->getMessages()]];

        return $this->build($exception->getContent(), $meta)->setStatusCode($exception->getHttpStatusCode());
    }

    protected function runAdapters($response, $meta)
    {
        $meta = $this->metaInterceptors->reduce(function($carry, MetadataInterceptor $interceptor) use($response) {
                return $interceptor->run($response, $carry);
        }, $meta);

        /** @var ResponseAdapter $adapter */
        $adapter = $this->adapters->first(function(ResponseAdapter $adapter) use($response, $meta) {
            return $adapter->isCompatible($response, $meta);
        });

        return $adapter ? $adapter->get($response, $meta) : $response;
    }

    public function addMiddleware($name, ResponseAdapter $middleware)
    {
        $this->adapters->put($name, $middleware);

        return $this;
    }

    public function removeMiddleware($name)
    {
        $this->adapters->forget($name);

        return $this;
    }
}