<?php


namespace Listery\Api\Http\Response\Adapters;


use Listery\Api\Transformers\StringTransformer;
use League\Fractal\Resource\Item;

class StringAdapter extends FractalAdapter
{
    public function get($response, $meta = null)
    {
        return $this->createDataWithTransformer(Item::class, new StringTransformer(), $response, $meta);
    }

    public function isCompatible($response, $meta = null)
    {
        return is_string($response);
    }

}