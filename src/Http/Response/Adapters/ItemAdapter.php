<?php


namespace Listery\Api\Http\Response\Adapters;


use Illuminate\Support\Collection;
use League\Fractal\Resource\Item;

class ItemAdapter extends FractalAdapter
{
    public function get($response, $meta = null)
    {
        return $this->createData(Item::class, $response, $meta);
    }

    public function isCompatible($response, $meta = null)
    {
        return !is_array($response) && is_object($response) && count($response) === 1 && !$response instanceof Collection;
    }
}