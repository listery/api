<?php


namespace Listery\Api\Http\Response\Adapters;


use Listery\Api\Resolution\Switchboard;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\ResourceAbstract;

abstract class FractalAdapter implements ResponseAdapter
{
    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var Switchboard
     */
    protected $switchboard;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Switchboard $switchboard, Manager $manager, Request $request)
    {
        $this->manager = $manager;
        $this->switchboard = $switchboard;
        $this->request = $request;
    }

    protected function getTransformer($content)
    {
        return $this->switchboard;
    }

    protected function createData($resourceType, $content, $meta = null)
    {
        $transformer = $this->getTransformer($content);

        if($transformer)
        {
            return $this->createDataWithTransformer($resourceType, $transformer, $content, $meta);
        }

        return $content;
    }

    protected function createDataWithTransformer($resourceType, $transformer, $content, $meta = null)
    {
        /** @var ResourceAbstract $resource */
        $resource = new $resourceType($content, $transformer);
        $resource->setMeta($meta ?? []);

        if($content instanceof LengthAwarePaginator)
        {
            $resource->setPaginator(new IlluminatePaginatorAdapter($content));
        }

        $this->manager->parseIncludes($this->request->get('include', []));
        $this->manager->parseExcludes($this->request->get('exclude', []));

        return $this->manager->createData($resource)->toArray();
    }
}