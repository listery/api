<?php


namespace Listery\Api\Http\Response\Adapters;


use Illuminate\Support\Collection;
use League\Fractal\Resource\Collection as FractalCollection;

class CollectionAdapter extends FractalAdapter
{
    public function get($response, $meta = null)
    {
        return $this->createData(FractalCollection::class, $response, $meta);
    }

    public function isCompatible($response, $meta = null)
    {
        return is_array($response) || $response instanceof Collection;
    }


}