<?php


namespace Listery\Api\Http\Response\Adapters;


use Symfony\Component\HttpFoundation\Response;

interface ResponseAdapter
{
    /**
     * @param $response
     * @param null $meta
     * @return Response
     */
    public function get($response, $meta = null);

    /**
     * @param $response
     * @param null $meta
     * @return boolean
     */
    public function isCompatible($response, $meta = null);
}