<?php


namespace Listery\Api\Http\Response\Adapters;


use Listery\Api\Transformers\NullTransformer;
use League\Fractal\Resource\Item;

class NullAdapter extends FractalAdapter
{
    public function get($response, $meta = null)
    {
        return $this->createDataWithTransformer(Item::class, new NullTransformer(), $response, $meta);
    }

    public function isCompatible($response, $meta = null)
    {
        return $response === null;
    }

}