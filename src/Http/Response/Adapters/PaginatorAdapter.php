<?php


namespace Listery\Api\Http\Response\Adapters;


use Illuminate\Contracts\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;

class PaginatorAdapter extends FractalAdapter
{
    public function get($response, $meta = null)
    {
        $resource = new Collection($response, $this->getTransformer($response));
//        $resource->setMeta($meta ?? []);
//        $resource->setPaginator(new IlluminatePaginatorAdapter($response));
//
//        return $this->manager->createData($resource)->toArray();
        return $this->createData(Collection::class, $response, $meta);
    }

    public function isCompatible($response, $meta = null)
    {
        return $response instanceof Paginator;
    }

}