<?php


namespace Listery\Api\Http\Response\Serializers;


use League\Fractal\Serializer\DataArraySerializer;

class DefaultSerializer extends DataArraySerializer
{
    public function item($resourceKey, array $data = null)
    {
        if(is_null($data))
        {
            $data = [];
        }

        return parent::item($resourceKey, $data);
    }
}