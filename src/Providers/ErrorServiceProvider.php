<?php


namespace Listery\Api\Providers;


use Listery\Api\Exceptions\ExceptionInterceptor;
use Listery\Api\Exceptions\Handler;
use Listery\Api\Exceptions\Interceptors\Interceptor;
use Listery\Api\Http\Contracts\ResponseFactory;
use Listery\Api\Http\Routing\Router;
use Listery\Api\Traits\ApiConfig;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;

class ErrorServiceProvider extends ServiceProvider
{
    use ApiConfig;

    public function boot()
    {

    }

    public function register()
    {
        $this->app->singleton(ExceptionHandler::class, $this->getConfig('exception_handler'));

        foreach($this->getConfig('exception_interceptors') as $interceptor)
        {
            $this->app->tag($interceptor, Interceptor::class);
        }

        $this->app->singleton(ExceptionInterceptor::class, function(Container $app){
            return new ExceptionInterceptor($app->tagged(Interceptor::class));
        });

        $this->app->bind(Handler::class, function (Container $app) {
            return new Handler(
                $app,
                $this->getConfig('errors'),
                $app->make(ResponseFactory::class),
                $app->make(ExceptionInterceptor::class),
                $app->make(Router::class)
            );
        });
    }
}