<?php


namespace Listery\Api\Transformers;


use League\Fractal\TransformerAbstract;

class NullTransformer extends TransformerAbstract
{
    public function transform()
    {
        return null;
    }
}