<?php


namespace Listery\Api\Transformers;


use League\Fractal\TransformerAbstract;

class StringTransformer extends TransformerAbstract
{
    public function transform(string $content)
    {
        return ['body' => $content];
    }
}