<?php


namespace Listery\Tests\App\Transformers;


use League\Fractal\TransformerAbstract;
use Listery\Tests\App\Entities\Foo;

class FooTransformer extends TransformerAbstract
{
    public function transform(Foo $foo)
    {
        return [
            'name' => $foo->data,
        ];
    }
}