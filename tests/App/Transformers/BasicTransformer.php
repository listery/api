<?php


namespace Listery\Tests\App\Transformers;


use League\Fractal\TransformerAbstract;

class BasicTransformer extends TransformerAbstract
{
    public function transform()
    {
        return [];
    }
}