<?php


namespace Listery\Tests\App\Transformers;


use League\Fractal\TransformerAbstract;

class UnresolveableTransformer extends TransformerAbstract
{
    protected $message;

    public function __construct($unknownDependency)
    {
        $this->message = $unknownDependency;
    }

    public function transform()
    {
        return [
            'message' => $this->message
        ];
    }
}