<?php


namespace Listery\Tests\App\Transformers;


use League\Fractal\TransformerAbstract;

class ComplexTransformer extends TransformerAbstract
{
    public function __construct(BasicTransformer $basicTransformer)
    {
        return array_merge(
            [
                'hi' => 'there'
            ],
            $basicTransformer->transform()
        );
    }
}