<?php


namespace Listery\Tests\App\Transformers;


use League\Fractal\TransformerAbstract;

class AnotherBasicTransformer extends TransformerAbstract
{
    public function transform($item)
    {
        return [];
    }
}