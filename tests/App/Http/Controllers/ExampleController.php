<?php


namespace Listery\Tests\App\Http\Controllers;


use Listery\Api\Http\Controller;
use Listery\Tests\App\Entities\Foo;

class ExampleController extends Controller
{
    public function index()
    {
        return new Foo();
    }
}