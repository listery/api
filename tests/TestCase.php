<?php


namespace Listery\Tests;

if(!defined('LARAVEL_START'))
{
    define('LARAVEL_START', microtime(true));
}

use Listery\Api\ApiServiceProvider;
use Listery\Api\Http\Middleware\ReturnsJson;
use Listery\Api\Http\Routing\Router;
use Exception;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Tests\TestCase as LaravelTestCase;
use PHPUnit_Framework_Assert as PHPUnit;

class TestCase extends LaravelTestCase
{
    /**
     * @var Response
     */
    protected $response;

    public function setUp()
    {
        parent::setUp();
        $this->app->register(ApiServiceProvider::class);
        $this->app->make('config')->set('app.debug', true);
        $this->app->extend('router', function(Router $router){
            $router->aliasMiddleware('json', ReturnsJson::class);
            return $router;
        });
    }

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $response = parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);
        $this->response = $response;

        return $response;
    }

    /**
     * Assert that the response contains the given JSON.
     *
     * @param  array  $data
     * @param  bool  $negate
     * @return $this
     */
    protected function seeJsonContains(array $data, $negate = false)
    {
        $method = $negate ? 'assertFalse' : 'assertTrue';
        $actual = json_encode(Arr::sortRecursive(
            (array) $this->decodeResponseJson()
        ));
        foreach (Arr::sortRecursive($data) as $key => $value) {
            $expected = $this->formatToExpectedJson($key, $value);
            $this->{$method}(
                Str::contains($actual, $expected),
                ($negate ? 'Found unexpected' : 'Unable to find').' JSON fragment'.PHP_EOL."[{$expected}]".PHP_EOL.'within'.PHP_EOL."[{$actual}]."
            );
        }
        return $this;
    }

    /**
     * Assert that the JSON response has a given structure.
     *
     * @param  array|null  $structure
     * @param  array|null  $responseData
     * @return $this
     */
    public function seeJsonStructure(array $structure = null, $responseData = null)
    {
        if (is_null($structure)) {
            return $this->seeJson();
        }
        if (is_null($responseData)) {
            $responseData = $this->decodeResponseJson();
        }
        foreach ($structure as $key => $value) {
            if (is_array($value) && $key === '*') {
                $this->assertInternalType('array', $responseData);
                foreach ($responseData as $responseDataItem) {
                    $this->seeJsonStructure($structure['*'], $responseDataItem);
                }
            } elseif (is_array($value)) {
                $this->assertArrayHasKey($key, $responseData);
                $this->seeJsonStructure($structure[$key], $responseData[$key]);
            } else {
                $this->assertArrayHasKey($value, $responseData);
            }
        }
        return $this;
    }

    /**
     * Assert that the client response has an OK status code.
     *
     * @return $this
     */
    public function assertResponseOk()
    {
        $actual = $this->response->getStatusCode();
        PHPUnit::assertTrue($this->response->isOk(), "Expected status code 200, got {$actual}.");
        return $this;
    }

    /**
     * Validate and return the decoded response JSON.
     *
     * @return array
     */
    protected function decodeResponseJson()
    {
        $decodedResponse = json_decode($this->response->getContent(), true);
        if (is_null($decodedResponse) || $decodedResponse === false) {
            $this->fail('Invalid JSON was returned from the route. Perhaps an exception was thrown?');
        }
        return $decodedResponse;
    }

    /**
     * Format the given key and value into a JSON string for expectation checks.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return string
     */
    protected function formatToExpectedJson($key, $value)
    {
        $expected = json_encode([$key => $value]);
        if (Str::startsWith($expected, '{')) {
            $expected = substr($expected, 1);
        }
        if (Str::endsWith($expected, '}')) {
            $expected = substr($expected, 0, -1);
        }
        return trim($expected);
    }

    /**
     * Assert that the response contains JSON.
     *
     * @param  array|null  $data
     * @param  bool  $negate
     * @return $this
     */
    public function seeJson(array $data = null, $negate = false)
    {
        if (is_null($data)) {
            $this->assertJson(
                $this->response->getContent(), "JSON was not returned from [{$this->response->currentUri}]."
            );
            return $this;
        }
        try {
            return $this->seeJsonEquals($data);
        } catch (\PHPUnit_Framework_ExpectationFailedException $e) {
            return $this->seeJsonContains($data, $negate);
        }
    }

    /**
     * Assert that the response contains an exact JSON array.
     *
     * @param  array  $data
     * @return $this
     */
    public function seeJsonEquals(array $data)
    {
        $actual = json_encode(Arr::sortRecursive(
            (array) $this->decodeResponseJson()
        ));
        $this->assertEquals(json_encode(Arr::sortRecursive($data)), $actual);
        return $this;
    }

    public function disableExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, new class extends \Illuminate\Foundation\Exceptions\Handler {
            public function __construct(){}
            public function report(Exception $e){}

            public function render($request, Exception $e)
            {
                throw $e;
            }
        });
    }

}