<?php


namespace Listery\Tests\Feature;


use Listery\Api\Resolution\Resolver;
use Listery\Tests\App\Entities\Foo;
use Listery\Tests\App\Transformers\AnotherBasicTransformer;
use Listery\Tests\App\Transformers\BasicTransformer;
use Listery\Tests\App\Transformers\ComplexTransformer;
use Listery\Tests\App\Transformers\FooTransformer;
use Listery\Tests\App\Transformers\UnresolveableTransformer;
use Listery\Tests\TestCase;

class AutoTransformerResolutionTest extends TestCase
{
    public function test_type_hinted_transformer_is_automatically_resolved()
    {
        $this->app->make('config')->set('api.transformers', [FooTransformer::class]);

        $resolver = $this->app->make(Resolver::class);
        $resolver->bind(Foo::class, FooTransformer::class);

        $this->assertInstanceOf(FooTransformer::class, $resolver->get(Foo::class));
    }

    public function test_it_auto_resolves_transformers_by_entity_given_in_transform_method()
    {
        $this->app->make('config')->set('api.transformers', [FooTransformer::class]);

        $resolver = $this->app->make(Resolver::class);

        $this->assertInstanceOf(FooTransformer::class, $resolver->get(Foo::class));
    }

    /**
     * @test
     */
    public function can_mix_and_match_forced_and_auto_resolved_transformers()
    {
        $this->app->make('config')->set('api.transformers', [
            BasicTransformer::class => CustomEntity::class,
            AnotherBasicTransformer::class => [AnotherEntity::class],
            FooTransformer::class
        ]);

        $resolver = $this->app->make(Resolver::class);

        $this->assertInstanceOf(FooTransformer::class, $resolver->get(Foo::class));
        $this->assertInstanceOf(BasicTransformer::class, $resolver->get(CustomEntity::class));
        $this->assertInstanceOf(AnotherBasicTransformer::class, $resolver->get(AnotherEntity::class));
    }
}

class CustomEntity {

}

class AnotherEntity {

}