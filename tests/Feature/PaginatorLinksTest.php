<?php


namespace Listery\Tests\Feature;


use App\Http\Controllers\Controller;
use Listery\Api\Http\Middleware\ReturnsJson;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Route;
use Listery\Tests\App\Entities\Foo;
use Listery\Tests\App\Transformers\FooTransformer;
use Listery\Tests\TestCase;

class PaginatorLinksTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->app->make('config')->set('api.transformers', [
            FooTransformer::class
        ]);
        $this->disableExceptionHandling();
    }

    public function test_it_includes_paginator_links_in_response()
    {
        Route::get('/test', PaginatorController::class . '@index')->middleware('json');

        $this->get('/test');

        $this
            ->seeJsonStructure(
                [
                    'meta' => [
                        'pagination' => [
                            'count', 'current_page', 'per_page', 'total', 'total_pages', 'links' => ['next']
                        ]
                    ]
                ]
            );
    }

    public function test_it_does_not_transform_for_unknown_entities_in_paginators_and_uses_default_response_instead()
    {
        Route::get('/test', PaginatorController::class . '@unknown')->middleware('json');
        $this->get('/test');

        $this->seeJsonStructure(
            [
                'meta' => ['pagination' => ['total', 'per_page']]
            ]
        );
    }
}

class PaginatorController extends Controller
{

    public function index()
    {
        return new LengthAwarePaginator(collect([new Foo(), new Foo()]), 2, 1);
    }

    public function unknown()
    {
        return new LengthAwarePaginator(collect([new PagDummy()]), 1, 1);
    }
}

class PagDummy {

}