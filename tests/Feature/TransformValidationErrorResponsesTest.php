<?php


namespace Listery\Tests\Feature;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Listery\Tests\TestCase;

class TransformValidationErrorResponsesTest extends TestCase
{
    public function test_validation_response_is_captured_and_transformed()
    {
        Route::get('/test', ValidationController::class . '@index')->middleware('json');

        $this->json('get', '/test?name=8');

        $this->seeJsonStructure(
            [
                'meta' => [
                    'errors' => [
                        '422' => [
                            'name'
                        ]
                    ]
                ]
            ]
        );

        $this->seeJson(
            [
                'name' =>
                    [
                        0 => "The name must be at least 2 characters.",
                        1 => "The name may only contain letters.",
                    ]
            ]
        );
    }
}

class ValidationController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|alpha'
        ]);

        return 'yes';
    }
}