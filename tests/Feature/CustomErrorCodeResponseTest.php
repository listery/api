<?php


namespace Listery\Tests\Feature;


use App\Http\Controllers\Controller;
use Listery\Api\Exceptions\Exception;
use Listery\Api\Http\Middleware\ReturnsJson;
use Illuminate\Support\Facades\Route;
use Listery\Tests\App\Entities\Foo;
use Listery\Tests\App\Transformers\FooTransformer;
use Listery\Tests\TestCase;

class CustomErrorCodeResponseTest extends TestCase
{
    public function test_a_custom_error_code_is_given_for_a_specific_exception()
    {
        $this->app->make('config')->set('api.errors', [
            CustomException::class => "999"
        ]);
        $this->app->make('config')->set('api.transformers', [
            FooTransformer::class
        ]);
        Route::get('/test', CustomController::class . '@index')->middleware('json');

        $this->get('/test');

        $this->seeJsonContains(
            [
                'errors' => [
                    "999" => ['MESSAGE']
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function http_status_code_is_returned()
    {
        $this->app->make('config')->set('api.errors', [
            CustomException::class => "999"
        ]);
        $this->app->make('config')->set('api.transformers', [
            FooTransformer::class
        ]);

        Route::get('/test', CustomController::class . '@index')->middleware('json');

        $this->get('/test');
        $this->assertEquals(500, $this->response->getStatusCode());
    }
}

class CustomException extends Exception
{
    public function getHttpStatusCode()
    {
        return 500;
    }

}

class CustomController extends Controller
{
    public function index()
    {
        throw new CustomException("MESSAGE", new Foo());
    }
}