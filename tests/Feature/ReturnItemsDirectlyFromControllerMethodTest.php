<?php


namespace Listery\Tests\Feature;


use App\Http\Controllers\Controller;
use Listery\Api\Resolution\Resolver;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Listery\Tests\App\Entities\Foo;
use Listery\Tests\App\Transformers\FooTransformer;
use Listery\Tests\TestCase;

class ReturnItemsDirectlyFromControllerMethodTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->app->make('config')->set('api.transformers', [FooTransformer::class]);
    }

    public function test_can_return_an_item_from_a_controller_action()
    {
        Route::get('/test', ExampleController::class . '@index')->middleware('json');

        $this->get('/test');

        $this
            ->assertResponseOk()
            ->seeJsonContains(
                [
                    'data' => [
                        'name' => 'foo'
                    ]
                ]
            );
    }

    public function test_can_return_string_from_controller()
    {
        Route::get('/test', ExampleController::class . '@text')->middleware('json');

        $this->get('/test');
        $this
            ->seeJsonStructure(
                [
                    'data' => [
                        'body'
                    ]
                ]
            );
    }

    public function test_can_return_collection_from_controller()
    {
        Route::get('/test', ExampleController::class . '@collection')->middleware('json');

        $this->get('/test');

        $this->seeJsonStructure(
            [
                'data' => [
                    ['name']
                ]
            ]
        );
    }
}

class ExampleController extends Controller
{
    public function index(Request $request)
    {
        return new Foo();
    }

    public function text()
    {
        return 'text';
    }

    public function collection()
    {
        return new Collection([new Foo()]);
    }
}