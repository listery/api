<?php


namespace Listery\Tests\Feature;


use App\Http\Controllers\Controller;
use Listery\Tests\TestCase;
use Illuminate\Support\Facades\Route;

class LaravelDefaultsTest extends TestCase
{
    /**
     * @test
     */
    public function does_not_interfere_with_normal_response_objects()
    {
        Route::get('/test', LaravelDefaultsController::class . '@json')->middleware('json');

        $this->get('/test');

        $this->assertResponseOk();
        $this->assertNotContains('HTML', $this->response->getContent());
        $this->seeJson(['hello' => 'world']);
    }

    /**
     * @test
     */
    public function does_not_interfere_with_redirects()
    {
        Route::get('/test', LaravelDefaultsController::class . '@redirect')->middleware('json');

        $this->get('/test');

        $this->assertEquals(302, $this->response->getStatusCode());
    }
}

class LaravelDefaultsController extends Controller {

    public function json()
    {
        return response()->json(['hello' => 'world']);
    }

    public function redirect()
    {
        return response()->redirectTo('/test');
    }
}