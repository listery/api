<?php


namespace Listery\Tests\Feature;


use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Listery\Tests\App\Transformers\BasicTransformer;
use Listery\Tests\App\Transformers\ComplexTransformer;
use Listery\Tests\App\Transformers\UnresolveableTransformer;
use Listery\Tests\TestCase;

class DefineTransformerServicesTest extends TestCase
{
    public function test_it_can_define_a_service_in_the_container()
    {
        $transformer = $this->app->make(BasicTransformer::class);

        $this->assertInstanceOf(BasicTransformer::class, $transformer);
    }

    public function test_resolves_transformers_with_dependencies()
    {
        $transformer = $this->app->make(ComplexTransformer::class);

        $this->assertInstanceOf(ComplexTransformer::class, $transformer);
    }

    public function test_can_manually_bind_transformers()
    {
        $this->expectException(BindingResolutionException::class);
        $this->app->make(UnresolveableTransformer::class);

        $this->app->bind(UnresolveableTransformer::class, function(Container $app){
            return new UnresolveableTransformer('hello_world');
        });

        $this->assertEquals(['message' => 'hello_world'], $this->app->make(UnresolveableTransformer::class)->transform());
    }
}