<?php


namespace Listery\Tests\Feature;


use Listery\Tests\TestCase;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class CanTransformIndividualObjectsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->app->make('config')->set('api.transformers', [ObjectOneTransformer::class, ObjectTwoTransformer::class, ObjectThreeTransformer::class]);
        $this->disableExceptionHandling();
    }

    /**
     * @test
     */
    public function can_transform_single_entities_of_different_type_in_collection()
    {
        Route::get('/test', IndividualObjectsController::class . '@collection')->middleware('json');

        $this->get('/test');

        $this->seeJson(
            [
                'data' => [
                    ['type' => 'one'],
                    ['type' => 'two'],
                    ['type' => 'three'],
                ]
            ]
        );
    }

    /**
     * @test
     */
    public function can_transform_with_includes()
    {
        Route::get('/test', IndividualObjectsController::class . '@includes')->middleware('json');

        $this->get('/test?include=something');

        $this->seeJson(
            [
                'data' => [
                    [
                        'type' => 'one',
                        'something' => [
                            'data' => ['type' => 'two']
                        ]
                    ],
                    ['type' => 'two'],
                    ['type' => 'three'],
                ]
            ]
        );
    }
}

class IndividualObjectsController extends Controller {

    public function collection()
    {
        $example1 = new ObjectOne();
        $example2 = new ObjectTwo();
        $example3 = new ObjectThree();

        return collect([$example1, $example2, $example3]);
    }

    public function includes()
    {
        $example1 = new ObjectOne();
        $example2 = new ObjectTwo();
        $example3 = new ObjectThree();

        return collect([$example1, $example2, $example3]);
    }
}

class ObjectOne {}
class ObjectTwo {}
class ObjectThree {}

class ObjectOneTransformer extends TransformerAbstract {

    public $availableIncludes = ['something'];

    public function transform(ObjectOne $one)
    {
        return ['type' => 'one'];
    }

    public function includeSomething()
    {
        return new Item(new ObjectTwo(), new ObjectTwoTransformer());
    }
}

class ObjectTwoTransformer extends TransformerAbstract {

    public function transform(ObjectTwo $two)
    {
        return ['type' => 'two'];
    }
}

class ObjectThreeTransformer extends TransformerAbstract {

    public function transform(ObjectThree $three)
    {
        return ['type' => 'three'];
    }
}