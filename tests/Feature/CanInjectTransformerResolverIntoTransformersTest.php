<?php


namespace Listery\Tests\Feature;


use Listery\Api\Resolution\TransformerResolver;
use Listery\Tests\TestCase;
use League\Fractal\TransformerAbstract;

class CanInjectTransformerResolverIntoTransformersTest extends TestCase
{
    /**
     * @var TransformerResolver
     */
    protected $resolver;

    public function setUp()
    {
        parent::setUp();
        $this->app->make('config')->set('api.transformers', [DependentTransformer::class => Object::class]);
        $this->disableExceptionHandling();
        $this->resolver = $this->app->make(TransformerResolver::class);
    }

    /**
     * @test
     */
    public function can_inject_resolver_in_to_transformers()
    {
        $this->assertInstanceOf(DependentTransformer::class, $transformer = $this->resolver->get(Object::class));
        $this->assertInstanceOf(TransformerResolver::class, $resolver = $transformer->getResolver());
        $this->assertEquals($this->resolver, $resolver);
    }

    /**
     * @test
     */
    public function transformer_still_resolves_other_transformers_after_injection()
    {
        $transformer = $this->resolver->get(Object::class);
        $this->assertInstanceOf(DependentTransformer::class, $transformer->getResolver()->get(Object::class));
    }
}

class Object {}

class DependentTransformer extends TransformerAbstract {

    /**
     * @var TransformerResolver
     */
    protected $resolver;

    public function __construct(TransformerResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function getResolver()
    {
        return $this->resolver;
    }

    public function transform($object)
    {
        return [];
    }
}

class AnotherDependent extends TransformerAbstract {

}