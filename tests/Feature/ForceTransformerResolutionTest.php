<?php


namespace Listery\Tests\Feature;


use Listery\Api\Resolution\Resolver;
use Listery\Tests\TestCase;
use League\Fractal\TransformerAbstract;

class ForceTransformerResolutionTest extends TestCase
{
    /**
     * @test
     */
    public function can_set_transformer_for_an_entity_without_auto_resolution()
    {
        $this->app->make('config')->set('api.transformers', [UnableToBeAutoResolvedTransformer::class => RandomEntity::class]);

        $resolver = $this->app->make(Resolver::class);

        $this->assertInstanceOf(UnableToBeAutoResolvedTransformer::class, $resolver->get(RandomEntity::class));
    }
}

class RandomEntity {

}

class UnableToBeAutoResolvedTransformer extends TransformerAbstract {

    public function transform()
    {
        return [];
    }

}